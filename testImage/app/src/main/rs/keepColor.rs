#pragma version(1)
#pragma rs java_package_name(com.example.qduart.testimage)

static const float4 weight = {0.299f, 0.587f, 0.114f, 0.0f};

uchar4 RS_KERNEL keepColor(uchar4 in) {

    const float4 pixelf = rsUnpackColor8888(in);
    float r = pixelf.r;
    float g = pixelf.g;
    float b = pixelf.b;

    if (r*255 < 240 && ( g*255 > 70 || b*255 > 70 )) {
        const float grey = dot(pixelf, weight);
        return rsPackColorTo8888(grey, grey, grey, pixelf.a);
    }

    else{
        return rsPackColorTo8888(r, g, b, pixelf.a);
    }
}