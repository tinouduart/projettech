package com.example.qduart.testimage;

import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.graphics.Bitmap;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.ImageView;
import android.widget.Button;
import android.view.View;
import android.support.v8.renderscript.*;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private Bitmap bmp;
    private Bitmap bmp2;
    private ImageView imgv;
    private ImageView imgv1;
    private int[] saved;

    // Variables permettant de savoir quelle fonction va être utilisée
    private boolean g, gs, grs, cstd, cconv, crs, ceq, cext, crgb, conv, k, krs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView tv = (TextView) findViewById(R.id.txt);

        bmp = BitmapFactory.decodeResource(getResources(), R.drawable.ecureuil);
        String size = bmp.getWidth() + "*" + bmp.getHeight();
        String s = "Size = " + size;
        tv.setText(s);
        saved = new int[bmp.getHeight()*bmp.getWidth()];
        saved = SaveIMG(bmp);

        imgv = (ImageView) findViewById(R.id.imageView);
        bmp2 = bmp.copy(Bitmap.Config.ARGB_8888, true);

        Button btn = (Button) findViewById(R.id.btn);

        //Bouton permettant de lancer les différentes fonctions
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (g) {
                    togrey(bmp2);
                }
                if (gs) {
                    togreys(bmp2);
                }
                if (grs) {
                    toGreyRS(bmp2);
                }
                if (cstd) {
                    Random r = new Random();
                    int value = r.nextInt(360);
                    Colorize(bmp, value);
                }
                if (cconv) {
                    Random r = new Random();
                    int value = r.nextInt(360);
                    colorizeM(bmp2, value);
                }
                if (crs)
                {
                    Random r = new Random();
                    int value = r.nextInt(360);
                    colorizeRs(bmp2, value);
                }
                if (ceq)
                {
                    Eqhistogram(bmp2);
                }
                if (cext)
                {
                    extHistogram(bmp2);
                }
                if (crgb)
                {
                    histRGB(bmp2);
                }
                if (conv)
                {
                    for(int i = 0; i < 20; i++)
                        convolutionBlur(bmp2, 3);
                }
                if (k)
                {
                    keepColor(bmp2);
                }
                if (krs)
                {
                    keepColorRs(bmp2);
                }
            }
        });
    }

    // Permet de créer un menu pour l'application
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    // On définit ce qui se passe lorque l'on clique sur chacune des options du menu
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id)
        {
            case R.id.greyscale:
                g = true;
                gs = false;
                grs = false;
                cstd = false;
                cconv = false;
                crs = false;
                ceq = false;
                cext = false;
                crgb = false;
                conv = false;
                k = false;
                krs = false;
                break;
            case R.id.greyscales:
                gs = true;
                g = false;
                grs = false;
                cstd = false;
                cconv = false;
                crs = false;
                ceq = false;
                cext = false;
                crgb = false;
                conv = false;
                k = false;
                krs = false;
                break;
            case R.id.greyscalers:
                grs = true;
                g = false;
                gs = false;
                cstd = false;
                cconv = false;
                crs = false;
                ceq = false;
                cext = false;
                crgb = false;
                conv = false;
                k = false;
                krs = false;
                break;
            case R.id.colorize:
                grs = false;
                g = false;
                gs = false;
                cstd = true;
                cconv = false;
                crs = false;
                ceq = false;
                cext = false;
                crgb = false;
                conv = false;
                k = false;
                krs = false;
                break;
            case R.id.colorizec:
                grs = false;
                g = false;
                gs = false;
                cstd = false;
                cconv = true;
                crs = false;
                ceq = false;
                cext = false;
                crgb = false;
                conv = false;
                k = false;
                krs = false;
                break;
            case R.id.colorizers:
                grs = false;
                g = false;
                gs = false;
                cstd = false;
                cconv = false;
                crs = true;
                ceq = false;
                cext = false;
                crgb = false;
                conv = false;
                k = false;
                krs = false;
                break;
            case R.id.invert:
                invert(bmp2);
            case R.id.contrasteq:
                grs = false;
                g = false;
                gs = false;
                cstd = false;
                cconv = false;
                crs = false;
                ceq = true;
                cext = false;
                crgb = false;
                conv = false;
                k = false;
                krs = false;
                break;
            case R.id.contrastext:
                grs = false;
                g = false;
                gs = false;
                cstd = false;
                cconv = false;
                crs = false;
                ceq = false;
                cext = true;
                crgb = false;
                conv = false;
                k = false;
                krs = false;
                break;
            case R.id.contrastrgb:
                grs = false;
                g = false;
                gs = false;
                cstd = false;
                cconv = false;
                crs = false;
                ceq = false;
                cext = false;
                crgb = true;
                conv = false;
                k = false;
                krs = false;
                break;
            case R.id.resetimg:
                Reset(bmp2);
                break;
            case R.id.convolution:
                grs = false;
                g = false;
                gs = false;
                cstd = false;
                cconv = false;
                crs = false;
                ceq = false;
                cext = false;
                crgb = false;
                conv = true;
                k = false;
                krs = false;
                break;
            case R.id.keepcolor:
                grs = false;
                g = false;
                gs = false;
                cstd = false;
                cconv = false;
                crs = false;
                ceq = false;
                cext = false;
                crgb = false;
                conv = false;
                k = true;
                krs = false;
                break;
            case R.id.keepcolorrs:
                grs = false;
                g = false;
                gs = false;
                cstd = false;
                cconv = false;
                crs = false;
                ceq = false;
                cext = false;
                crgb = false;
                conv = false;
                k = false;
                krs = true;
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    // Permet de convertir une image en niveau de gris en utilisant les fonction getPixels et setPixels
    public void togreys(Bitmap bmp2) {
        int size = bmp2.getWidth() * bmp2.getHeight();
        int[] p = new int[size];

        bmp2.getPixels(p, 0, bmp2.getWidth() , 0, 0, bmp2.getWidth(), bmp2.getHeight());

        for(int i = 0; i < size; i++)
        {
            int r = Color.red(p[i]);
            int g = Color.green(p[i]);
            int b = Color.blue(p[i]);

            double grey = 0.3 * r + 0.59 * g + 0.11 * b;
            p[i] = Color.rgb((int)grey, (int)grey, (int)grey);
        }
        bmp2.setPixels(p, 0, bmp2.getWidth(),0,0,bmp2.getWidth(), bmp2.getHeight());
        imgv.setImageBitmap(bmp2);
    }

    // Permet de convertir une image en niveau de gris en utilisant les fonction getPixel et setPixel
    public void togrey(Bitmap bmp2) {
        for (int i = 0; i < bmp2.getWidth(); i++)
        {
            for (int j = 0; j < bmp2.getHeight(); j++)
            {
                int c = bmp2.getPixel(i, j);
                int r = Color.red(c);
                int g = Color.green(c);
                int b = Color.blue(c);

                double grey = 0.3 * r + 0.59 * g + 0.11 * b;
                c = Color.rgb((int)grey, (int)grey, (int)grey);
                bmp2.setPixel(i, j, c);
            }
        }
        imgv.setImageBitmap(bmp2);
    }

    // Version renderscript du niveau de gris
    public void toGreyRS(Bitmap bmp) {
        RenderScript rs = RenderScript.create(this);

        Allocation input = Allocation.createFromBitmap(rs, bmp);
        Allocation output = Allocation.createTyped(rs, input.getType());

        ScriptC_toGrey greyscript = new ScriptC_toGrey(rs);
        greyscript.forEach_toGrey(input, output);

        output.copyTo(bmp);
        input.destroy(); output.destroy();
        greyscript.destroy(); rs.destroy();
        imgv.setImageBitmap(bmp);
    }

    // Fonction invert, non demandée, permet d'inverser les couleurs d'une image
    public void invert(Bitmap bmp) {
        int size = bmp.getWidth() * bmp.getHeight();
        int[] p = new int[size];

        bmp.getPixels(p, 0, bmp.getWidth() , 0, 0, bmp.getWidth(), bmp.getHeight());

        for(int i = 0; i < size; i++)
        {
            int r = 255 - Color.red(p[i]);
            int g = 255 - Color.green(p[i]);
            int b = 255 - Color.blue(p[i]);

            //double grey = 0.3 * r + 0.59 * g + 0.11 * b;
            p[i] = Color.rgb(r, g, b);
        }
        bmp.setPixels(p, 0, bmp.getWidth(),0,0,bmp.getWidth(), bmp.getHeight());
        imgv.setImageBitmap(bmp2);
    }

    // Fonction permettant de changer la teinte d'une image. On effectue nous même la conversion
    public void colorizeM(Bitmap bmp, int value) {
        int size = bmp.getWidth() * bmp.getHeight();
        int[] p = new int[size];

        bmp.getPixels(p, 0, bmp.getWidth(), 0, 0, bmp.getWidth(), bmp.getHeight());
        for (int i = 0; i < size; i++)
        {
            float r = Color.red(p[i]);
            float g = Color.green(p[i]);
            float b = Color.blue(p[i]);

            // RGB TO HSV
            float r1 = r/255;
            float g1 = g/255;
            float b1 = b/255;
            //System.out.println("r1 = " + r1 + " g1 = " + g1 + " b1 = " + b1);

            float Cmax = Math.max(r1, g1);
            Cmax = Math.max(Cmax, b1);
            float Cmin = Math.min(r1, g1);
            Cmin = Math.min(Cmin, b1);

            //System.out.println("cmax = " + Cmax + " cmin = " + Cmin);

            float Delta = Cmax - Cmin;

            float H = value;

            float S;
            if (Cmax == 0) {
                S = 0;
            }
            else {
                S = Delta / Cmax;
            }

            float V = Cmax;

            // HSV TO RGB

            float C = V * S;
            float X = C * (1 - Math.abs((H/60)%2 - 1));

            float m = V - C;
            float r2 = 0;
            float g2 = 0;
            float b2 = 0;

            if (H >= 0 && H < 60){
                r2 = C;
                g2 = X;
                b2 = 0;
            }
            if (H >= 60 && H < 120){
                r2 = X;
                g2 = C;
                b2 = 0;
            }
            if (H >= 120 && H < 180){
                r2 = 0;
                g2 = C;
                b2 = X;
            }
            if (H >= 180 && H < 240){
                r2 = 0;
                g2 = X;
                b2 = C;
            }
            if (H >= 240 && H < 300){
                r2 = X;
                g2 = 0;
                b2 = C;
            }
            if (H >= 300 && H < 360){
                r2 = C;
                g2 = 0;
                b2 = X;
            }

            float r3;
            float g3;
            float b3;

            r3 = (r2 + m) * 255;
            g3 = (g2 + m) * 255;
            b3 = (b2 + m) * 255;
            p[i] = Color.rgb((int)r3,(int)g3,(int)b3);
        }
        bmp.setPixels(p, 0, bmp.getWidth(),0,0,bmp.getWidth(), bmp.getHeight());
        imgv.setImageBitmap(bmp);
    }

    // Permet de changer la teinte en utilisant les fonctions préexistantes RGBToHSV et HSVToColor
    public void Colorize(Bitmap bmp, int value) {
        int size = bmp.getWidth() * bmp.getHeight();
        int[] p = new int[size];

        bmp.getPixels(p, 0, bmp.getWidth(), 0, 0, bmp.getWidth(), bmp.getHeight());

        for (int i = 0; i < size; i++)
        {
            int r = Color.red(p[i]);
            int g = Color.green(p[i]);
            int b = Color.blue(p[i]);

            float[] hsv = new float[3];
            Color.RGBToHSV(r, g, b, hsv);
            hsv[0] = value;
            p[i] = Color.HSVToColor(hsv);
        }
        bmp.setPixels(p, 0, bmp.getWidth(), 0, 0, bmp.getWidth(), bmp.getHeight());
        imgv.setImageBitmap(bmp);
    }

    // Version renderscript de colorize
    public void colorizeRs(Bitmap bmp, int value) {
        RenderScript rs = RenderScript.create(this);
        Allocation input = Allocation.createFromBitmap(rs, bmp);
        Allocation output = Allocation.createTyped(rs, input.getType());

        ScriptC_colorize colorize = new ScriptC_colorize(rs);
        colorize.set_H(value);
        colorize.forEach_colorize(input, output);

        output.copyTo(bmp);

        input.destroy(); output.destroy();
        colorize.destroy(); rs.destroy();
        imgv.setImageBitmap(bmp);
    }

    // Permet de ne garder qu'une couleur
    public void keepColor(Bitmap bmp) {
        int size = bmp.getWidth() * bmp.getHeight();
        int[] p = new int[size];

        bmp.getPixels(p, 0, bmp.getWidth() , 0, 0, bmp.getWidth(), bmp.getHeight());

        for(int i = 0; i < size; i++)
        {
            if (Color.red(p[i]) < 240 && Color.green(p[i]) > 70 || Color.blue(p[i]) > 70) {

                int r = Color.red(p[i]);
                int g = Color.green(p[i]);
                int b = Color.blue(p[i]);

                double grey = 0.3 * r + 0.59 * g + 0.11 * b;
                p[i] = Color.rgb((int)grey, (int)grey, (int)grey);
            }
        }
        bmp.setPixels(p, 0, bmp.getWidth(),0,0,bmp.getWidth(), bmp.getHeight());
        imgv.setImageBitmap(bmp);
    }

    // Version renderscript de keepColor
    public void keepColorRs(Bitmap bmp) {
        RenderScript rs = RenderScript.create(this);
        Allocation input = Allocation.createFromBitmap(rs, bmp);
        Allocation output = Allocation.createTyped(rs, input.getType());

        ScriptC_keepColor keepcolor = new ScriptC_keepColor(rs);
        keepcolor.forEach_keepColor(input, output);

        output.copyTo(bmp);

        input.destroy(); output.destroy();
        keepcolor.destroy(); rs.destroy();
        imgv.setImageBitmap(bmp);
    }

    // Fonction faisant une égalisation d'histogramme
    public void Eqhistogram(Bitmap bmp) {

        int size = bmp.getWidth() * bmp.getHeight();
        int[] p = new int[size];

        int[] hist = new int[256];
        int[] CHist = new int[256];

        // Create histogram
        bmp.getPixels(p, 0, bmp.getWidth() , 0, 0, bmp.getWidth(), bmp.getHeight());
         for (int i = 0; i < size; i++)
         {
             hist[Color.red(p[i])]++;
         }

        CHist[0] = hist[0];
        for (int i = 1; i < 256; i++)
            CHist[i] = CHist[i-1] + hist[i];


        int[] T = new int[256];

        for (int i = 0; i < 256; i++) {
            T[i] = Math.round((255 * CHist[i]) / size);
            T[i] = Color.rgb(T[i], T[i], T[i]);
        }

        for (int i = 0; i < size; i++)
        {
            p[i] = T[Color.red(p[i])];
        }
        bmp.setPixels(p, 0, bmp.getWidth(), 0, 0, bmp.getWidth(), bmp.getHeight());
        imgv.setImageBitmap(bmp);

    }

    // Fonction qui fait une extension de dynamique
    public void extHistogram(Bitmap bmp) {
        int size = bmp.getWidth() * bmp.getHeight();
        int[] p = new int[size];

        int[] hist = new int[256];
        bmp.getPixels(p, 0, bmp.getWidth(), 0, 0, bmp.getWidth(), bmp.getHeight());

        // Create histogram
        for (int i = 0; i < size; i++) {
            hist[Color.red(p[i])]++;
        }

        // find min and max
        int min = 0;
        while (hist[min] == 0)
            min++;
        int max = min;
        while (hist[max] != 0)
            max++;

        if (max == min)
            return;

        // Look Up Table init
        int[] LUT = new int[256];
        for (int ng = 0; ng < 256; ng++) {
            LUT[ng] = 255 * (ng - min) / (max - min);
            LUT[ng] = Color.rgb(LUT[ng], LUT[ng], LUT[ng]);
        }

        // Set new color
        for (int i = 0; i < size; i++)
            p[i] = LUT[Color.red(p[i])];

        bmp.setPixels(p, 0, bmp.getWidth(), 0, 0, bmp.getWidth(), bmp.getHeight());
        imgv.setImageBitmap(bmp);
    }

    // Calcul d'un histogramme RGB (NE FONCTIONNE PAS CORRECTEMENT) avec l'extension de dynamique
    public void histRGB(Bitmap bmp) {
        int size = bmp.getWidth() * bmp.getHeight();
        int[] p = new int[size];
        int[] hist = new int[256];
        int[] p2 = new int[size];
        bmp.getPixels(p2, 0, bmp.getWidth(), 0, 0, bmp.getWidth(), bmp.getHeight());
        bmp.getPixels(p, 0, bmp.getWidth(), 0, 0, bmp.getWidth(), bmp.getHeight());

        //greyscale
       for(int i = 0; i < size; i++)
       {
           int r = Color.red(p[i]);
           int g = Color.green(p[i]);
           int b = Color.blue(p[i]);

           int grey  = (r+g+b)/3;
           p[i] = Color.rgb(grey, grey, grey);
       }
        // Create histogram
        for (int i = 0; i < size; i++) {
            hist[Color.red(p[i])]++;
        }

        // find min and max
        int min = 0;
        while (hist[min] == 0)
            min++;
        int max = min;
        while (hist[max] != 0)
            max++;

        if (max == min)
            return;

        // Look Up Table init
        int[] LUT1 = new int[256];
        for (int r1 = 0; r1 < 256; r1++) {
            LUT1[r1] = 255 * (r1 - min) / (max - min);
        }
        int[] LUT2 = new int[256];
        for (int g1 = 0; g1 < 256; g1++) {
            LUT2[g1] = 255 * (g1 - min) / (max - min);
        }
        int[] LUT3 = new int[256];
        for (int b1 = 0; b1 < 256; b1++) {
            LUT3[b1] = 255 * (b1 - min) / (max - min);
        }
        int[] LUT = new int[256];

        // Set new color
        for (int i = 0; i < size; i++) {
            p2[i] = Color.rgb(LUT1[Color.red(p2[i])], LUT2[Color.green(p2[i])], LUT3[Color.blue(p2[i])]);
        }
        bmp.setPixels(p2, 0, bmp.getWidth(), 0, 0, bmp.getWidth(), bmp.getHeight());
        imgv.setImageBitmap(bmp);
    }

    // Permet de sauvegarder une image
    public int[] SaveIMG(Bitmap bmp) {
        int size = bmp.getWidth() * bmp.getHeight();
        int[] p = new int[size];

        bmp.getPixels(p, 0, bmp.getWidth() , 0, 0, bmp.getWidth(), bmp.getHeight());
        return p;
    }

    // Sert à reset l'image affichée
    public void Reset(Bitmap bmp) {
        bmp.setPixels(saved, 0, bmp.getWidth(), 0, 0, bmp.getWidth(), bmp.getHeight());
        imgv.setImageBitmap(bmp);
    }

    // Permet de faire une flou sur une image avec un masque de taille 3x3
    public void convolutionBlur (Bitmap bmp, int radius) {
        int w = bmp.getWidth();
        int h = bmp.getHeight();
        int size = w * h;

        int[] moy = new int[size];

        int[] p = new int[size];
        bmp.getPixels(p, 0, bmp.getWidth(), 0, 0, bmp.getWidth(), bmp.getHeight());

        int r = 0;
        int g = 0;
        int b = 0;
        for (int i = 1; i < h-1; i++)
        {
            for (int j = 1; j < w-1; j++)
            {
                for (int x = 0; x < radius; x++)
                {
                    r = Color.red(p[i*w + j]) + Color.red(p[i*w + j+1]) + Color.red(p[i*w + j-1]);
                    r += Color.red(p[(i-1) * w + j]) + Color.red(p[(i-1)*w + j+1]) + Color.red(p[(i-1)*w + j-1]);
                    r += Color.red(p[(i+1)*w + j]) + Color.red(p[(i+1)*w + j+1]) + Color.red(p[(i+1)*w + j-1]);
                    r /= 9;

                    g = Color.green(p[i*w + j]) + Color.green(p[i*w + j+1]) + Color.green(p[i*w + j-1]);
                    g += Color.green(p[(i-1)*w + j]) + Color.green(p[(i-1)*w + j+1]) + Color.green(p[(i-1)*w + j-1]);
                    g += Color.green(p[(i+1)*w + j]) + Color.green(p[(i+1)*w + j+1]) + Color.green(p[(i+1)*w + j-1]);
                    g /= 9;

                    b = Color.blue(p[i*w + j]) + Color.blue(p[i*w + j+1]) + Color.blue(p[i*w + j-1]);
                    b += Color.blue(p[(i-1)*w + j]) + Color.blue(p[(i-1)*w + j+1]) + Color.blue(p[(i-1)*w + j-1]);
                    b += Color.blue(p[(i+1)*w + j]) + Color.blue(p[(i+1)*w + j+1]) + Color.blue(p[(i+1)*w + j-1]);
                    b /= 9;

                    moy[i*w+j] = Color.rgb(r, g, b);
                }

            }
        }
        bmp.setPixels(moy, 0, bmp.getWidth(), 0, 0, bmp.getWidth(), bmp.getHeight());
        imgv.setImageBitmap(bmp);
    }
}